<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Coffee</title>
  <short_name>coffee</short_name>
  <dc:creator>michaelmol</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/coffee</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>coffee 7.x-2.4</name>
      <version>7.x-2.4</version>
      <tag>7.x-2.4</tag>
      <version_major>2</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/coffee/-/releases/7.x-2.4</release_link>
      <download_link>https://gitlab.com/api/v4/projects/55398429/packages/generic/coffee/7.x-2.4/coffee-7.x-2.4.tar.gz</download_link>
      <date>1709158586</date>
      <mdhash>00ef8a69fc0e64ac06c55c76891df85d</mdhash>
      <filesize>17990</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/55398429/packages/generic/coffee/7.x-2.4/coffee-7.x-2.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>00ef8a69fc0e64ac06c55c76891df85d</md5>
          <size>17990</size>
          <filedate>1709158586</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/55398429/packages/generic/coffee/7.x-2.4/coffee-7.x-2.4.tar.gz</url>
          <archive_type>zip</archive_type>
          <md5>b4c4da330f841042a1e65a14ee6c0d3f</md5>
          <size>21717</size>
          <filedate>1709158586</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Security update</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>coffee 7.x-2.3</name>
      <version>7.x-2.3</version>
      <tag>7.x-2.3</tag>
      <version_major>2</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/coffee/releases/7.x-2.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/coffee-7.x-2.3.tar.gz</download_link>
      <date>1477415039</date>
      <mdhash>905e52150920c6b526f09cf2fecbd39e</mdhash>
      <filesize>18062</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>905e52150920c6b526f09cf2fecbd39e</md5>
          <size>18062</size>
          <filedate>1477415039</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>217dd05d71d1b680fbd05a2cff79c267</md5>
          <size>21469</size>
          <filedate>1477415039</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>coffee 7.x-2.2</name>
      <version>7.x-2.2</version>
      <tag>7.x-2.2</tag>
      <version_major>2</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/coffee/releases/7.x-2.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/coffee-7.x-2.2.tar.gz</download_link>
      <date>1396514346</date>
      <mdhash>3721592e882a75508a37366ecb75a746</mdhash>
      <filesize>14497</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>3721592e882a75508a37366ecb75a746</md5>
          <size>14497</size>
          <filedate>1396514346</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>0a791510ef9033ef22bc179251dacbd8</md5>
          <size>17205</size>
          <filedate>1396514346</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>coffee 7.x-2.1</name>
      <version>7.x-2.1</version>
      <tag>7.x-2.1</tag>
      <version_major>2</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/coffee/releases/7.x-2.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/coffee-7.x-2.1.tar.gz</download_link>
      <date>1395405857</date>
      <mdhash>841ea67a24e2a5b79b543ff7c005fb36</mdhash>
      <filesize>14435</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>841ea67a24e2a5b79b543ff7c005fb36</md5>
          <size>14435</size>
          <filedate>1395405857</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>761ce0ff5494981b9b42477f75d53e66</md5>
          <size>17131</size>
          <filedate>1395405857</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>coffee 7.x-2.0</name>
      <version>7.x-2.0</version>
      <tag>7.x-2.0</tag>
      <version_major>2</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/coffee/releases/7.x-2.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/coffee-7.x-2.0.tar.gz</download_link>
      <date>1361998837</date>
      <mdhash>831b5eddf35aa77c8c9523a4f41fc269</mdhash>
      <filesize>14377</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>831b5eddf35aa77c8c9523a4f41fc269</md5>
          <size>14377</size>
          <filedate>1361998837</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>dbdafe48e1f12c95bcf3ce17ee3640cc</md5>
          <size>17052</size>
          <filedate>1361998837</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>coffee 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/coffee/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/coffee-7.x-1.0.tar.gz</download_link>
      <date>1328101860</date>
      <mdhash>25f82788540a5878471f73974b284af7</mdhash>
      <filesize>12865</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>25f82788540a5878471f73974b284af7</md5>
          <size>12865</size>
          <filedate>1328101860</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>9babb1f207f34a1b9c6a85cb35d8dbfc</md5>
          <size>14741</size>
          <filedate>1328101860</filedate>
        </file>
      </files>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>coffee 7.x-2.x-dev</name>
      <version>7.x-2.x-dev</version>
      <tag>7.x-2.x</tag>
      <version_major>2</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/coffee/releases/7.x-2.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/coffee-7.x-2.x-dev.tar.gz</download_link>
      <date>1508836444</date>
      <mdhash>abcc90e36503fe27ca2006b86965051d</mdhash>
      <filesize>17747</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>abcc90e36503fe27ca2006b86965051d</md5>
          <size>17747</size>
          <filedate>1508836444</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/coffee-7.x-2.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>a6be98370f69cd0044de8515c016194b</md5>
          <size>21474</size>
          <filedate>1508836444</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
