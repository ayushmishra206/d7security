<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>Simple Google Maps</title>
  <short_name>simple_gmap</short_name>
  <dc:creator>martin107</dc:creator>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/simple_gmap</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Actively maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
    <term>
      <name>Module categories</name>
      <value>Site Structure</value>
    </term>
  </terms>
  <releases>
    <release>
      <name>simple_gmap 7.x-1.6</name>
      <version>7.x-1.6</version>
      <tag>7.x-1.6</tag>
      <version_major>1</version_major>
      <version_patch>6</version_patch>
      <status>published</status>
      <release_link>https://gitlab.com/d7security/simple_gmap/-/releases/7.x-1.6</release_link>
      <download_link>https://gitlab.com/api/v4/projects/55392763/packages/generic/simple_gmap/7.x-1.6/simple_gmap-7.x-1.6.tar.gz</download_link>
      <date>1709171427</date>
      <mdhash>bad051cf5fee75d11d47bb9e7b058c92</mdhash>
      <filesize>13363</filesize>
      <files>
        <file>
          <url>https://gitlab.com/api/v4/projects/55392763/packages/generic/simple_gmap/7.x-1.6/simple_gmap-7.x-1.6.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bad051cf5fee75d11d47bb9e7b058c92</md5>
          <size>13363</size>
          <filedate>1709171427</filedate>
        </file>
        <file>
          <url>https://gitlab.com/api/v4/projects/55392763/packages/generic/simple_gmap/7.x-1.6/simple_gmap-7.x-1.6.tar.gz</url>
          <archive_type>zip</archive_type>
          <md5>a22017256e0cd0bdb9b544608fd65ba3</md5>
          <size>15261</size>
          <filedate>1709171427</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by D7Security's security policy</security>
    </release>
    <release>
      <name>simple_gmap 7.x-1.5</name>
      <version>7.x-1.5</version>
      <tag>7.x-1.5</tag>
      <version_major>1</version_major>
      <version_patch>5</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/simple_gmap/releases/7.x-1.5</release_link>
      <download_link>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.5.tar.gz</download_link>
      <date>1622931554</date>
      <mdhash>bd3d3c4736cbdcff8ca8bb42f5c4e926</mdhash>
      <filesize>12961</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.5.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bd3d3c4736cbdcff8ca8bb42f5c4e926</md5>
          <size>12961</size>
          <filedate>1622931554</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.5.zip</url>
          <archive_type>zip</archive_type>
          <md5>f65693174370770a658da55856879782</md5>
          <size>14737</size>
          <filedate>1622931554</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>simple_gmap 7.x-1.4</name>
      <version>7.x-1.4</version>
      <tag>7.x-1.4</tag>
      <version_major>1</version_major>
      <version_patch>4</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/simple_gmap/releases/7.x-1.4</release_link>
      <download_link>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.4.tar.gz</download_link>
      <date>1485532684</date>
      <mdhash>8b93eac3efb3472a7414156d42e7e8e1</mdhash>
      <filesize>11216</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.4.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>8b93eac3efb3472a7414156d42e7e8e1</md5>
          <size>11216</size>
          <filedate>1485532684</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.4.zip</url>
          <archive_type>zip</archive_type>
          <md5>74389d202168c922ca2d89c2ae8be6d1</md5>
          <size>12509</size>
          <filedate>1485532684</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>simple_gmap 7.x-1.3</name>
      <version>7.x-1.3</version>
      <tag>7.x-1.3</tag>
      <version_major>1</version_major>
      <version_patch>3</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/simple_gmap/releases/7.x-1.3</release_link>
      <download_link>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.3.tar.gz</download_link>
      <date>1479334937</date>
      <mdhash>6ccc5a629790068aedaa9c9a6c9fc07f</mdhash>
      <filesize>11175</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.3.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>6ccc5a629790068aedaa9c9a6c9fc07f</md5>
          <size>11175</size>
          <filedate>1479334937</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.3.zip</url>
          <archive_type>zip</archive_type>
          <md5>846db1ed758c1e915c3f3a81b113f41b</md5>
          <size>12484</size>
          <filedate>1479334937</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>simple_gmap 7.x-1.2</name>
      <version>7.x-1.2</version>
      <tag>7.x-1.2</tag>
      <version_major>1</version_major>
      <version_patch>2</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/simple_gmap/releases/7.x-1.2</release_link>
      <download_link>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.2.tar.gz</download_link>
      <date>1388781543</date>
      <mdhash>75806d7289284b9e98f909bf002d2a10</mdhash>
      <filesize>11099</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.2.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>75806d7289284b9e98f909bf002d2a10</md5>
          <size>11099</size>
          <filedate>1388781543</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.2.zip</url>
          <archive_type>zip</archive_type>
          <md5>8921e469a250b177f1a1e52099d1eec0</md5>
          <size>12239</size>
          <filedate>1388781543</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>simple_gmap 7.x-1.1</name>
      <version>7.x-1.1</version>
      <tag>7.x-1.1</tag>
      <version_major>1</version_major>
      <version_patch>1</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/simple_gmap/releases/7.x-1.1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.1.tar.gz</download_link>
      <date>1382573429</date>
      <mdhash>b5c8130366896782ef7a417fcca3cf0e</mdhash>
      <filesize>10726</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>b5c8130366896782ef7a417fcca3cf0e</md5>
          <size>10726</size>
          <filedate>1382573429</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.1.zip</url>
          <archive_type>zip</archive_type>
          <md5>70091e16ba0f6cf3230c61d87bcbce88</md5>
          <size>11842</size>
          <filedate>1382573430</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
        <term>
          <name>Release type</name>
          <value>Bug fixes</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>simple_gmap 7.x-1.0</name>
      <version>7.x-1.0</version>
      <tag>7.x-1.0</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <status>published</status>
      <release_link>https://www.drupal.org/project/simple_gmap/releases/7.x-1.0</release_link>
      <download_link>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.0.tar.gz</download_link>
      <date>1348681791</date>
      <mdhash>bf1784bfd667d939b799707447707b2f</mdhash>
      <filesize>10672</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.0.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>bf1784bfd667d939b799707447707b2f</md5>
          <size>10672</size>
          <filedate>1348681791</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.0.zip</url>
          <archive_type>zip</archive_type>
          <md5>b2cbe51d5e18ba8ffb5e361d17a21640</md5>
          <size>11788</size>
          <filedate>1348681791</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security covered="1">Covered by Drupal's security advisory policy</security>
    </release>
    <release>
      <name>simple_gmap 7.x-1.0-rc1</name>
      <version>7.x-1.0-rc1</version>
      <tag>7.x-1.0-rc1</tag>
      <version_major>1</version_major>
      <version_patch>0</version_patch>
      <version_extra>rc1</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/simple_gmap/releases/7.x-1.0-rc1</release_link>
      <download_link>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.0-rc1.tar.gz</download_link>
      <date>1329102042</date>
      <mdhash>e3ed0124056459fc58bb0b47e4191a9d</mdhash>
      <filesize>10122</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.0-rc1.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>e3ed0124056459fc58bb0b47e4191a9d</md5>
          <size>10122</size>
          <filedate>1329102042</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.0-rc1.zip</url>
          <archive_type>zip</archive_type>
          <md5>bf079cec53454480dfe02d68a8f76530</md5>
          <size>11185</size>
          <filedate>1329102042</filedate>
        </file>
      </files>
      <terms>
        <term>
          <name>Release type</name>
          <value>New features</value>
        </term>
      </terms>
      <security>RC releases are not covered by Drupal security advisories.</security>
    </release>
    <release>
      <name>simple_gmap 7.x-1.x-dev</name>
      <version>7.x-1.x-dev</version>
      <tag>7.x-1.x</tag>
      <version_major>1</version_major>
      <version_extra>dev</version_extra>
      <status>published</status>
      <release_link>https://www.drupal.org/project/simple_gmap/releases/7.x-1.x-dev</release_link>
      <download_link>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.x-dev.tar.gz</download_link>
      <date>1622931305</date>
      <mdhash>ae6a81f2e791ce537a2555da1d43cde8</mdhash>
      <filesize>12965</filesize>
      <files>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.x-dev.tar.gz</url>
          <archive_type>tar.gz</archive_type>
          <md5>ae6a81f2e791ce537a2555da1d43cde8</md5>
          <size>12965</size>
          <filedate>1622931305</filedate>
        </file>
        <file>
          <url>https://ftp.drupal.org/files/projects/simple_gmap-7.x-1.x-dev.zip</url>
          <archive_type>zip</archive_type>
          <md5>5fd7d220f5d8ca49238dc42f94e1b8ee</md5>
          <size>14742</size>
          <filedate>1622931305</filedate>
        </file>
      </files>
      <security>Dev releases are not covered by Drupal security advisories.</security>
    </release>
  </releases>
</project>
