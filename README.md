# D7Security - Drupal 7 Long Term Support (LTS)

The D7Security group provides unofficial extended support Drupal 7. We are working on Drupal 7 Long Term Support (LTS) to continue security and maintenance updates for Drupal Core and contributed projects now that Drupal 7 will reach end of life (EOL) on Drupal.org.

Please check the documentation in the [Wiki](https://gitlab.com/d7security/d7security/-/wikis/Home).

🚨 All security reports against Drupal 7 should still be reported via [security.drupal.org](https://security.drupal.org/node/add/project-issue/drupal) first! 🚨
