<?php

/**
 * @file
 * Generates the update XML file for d7security projects.
 *
 * Usage:
 *   php generate-update-xml.php --project-name=PROJECT_NAME --tag=TAG
 *
 * Example:
 *   php generate-update-xml.php --project-name=devel --tag=7.x-1.1
 *
 * If the script is invoked without arguments it will generate the update XML
 * for all recent releases in the D7Security group. Requires the ACCESS_TOKEN
 * environment variable to be set to Gitlab API token.
 */

$arguments = getopt('', ['project-name:', 'tag:']);
$project_name = $arguments['project-name'] ?? '';
$tag = $arguments['tag'] ?? '';

if (empty($project_name) || empty($tag)) {
  // If no concrete project is provided get the list of most recent releases in
  // the D7Security group.
  $token = getenv('ACCESS_TOKEN');
  $all_releases = json_decode(file_get_contents("https://gitlab.com/api/v4/groups/d7security/releases?private_token=$token"), TRUE);
  if (empty($all_releases)) {
    echo "Could not retrieve list of releases from Gitlab.\n";
    exit(1);
  }
  $supported_projects = explode("\n", file_get_contents(__DIR__ . '/supported_projects.txt'));
  foreach ($all_releases as $release) {
    preg_match('#/d7security/([^/]+)/#', $release['tag_path'], $matches);
    $project_name = $matches[1] ?? '';
    if ($project_name && in_array($project_name, $supported_projects)) {
      d7security_generate_single_xml($project_name, $release['tag_name']);
    }
  }
}
else {
  d7security_generate_single_xml($project_name, $tag);
}

/**
 * Generates the update XML file for a single project.
 */
function d7security_generate_single_xml(string $project_name, string $tag): void {
  print "Generating update XML for $project_name $tag.\n";

  $versions = d7security_get_major_minor_version($tag);

  // First check if there is already an update XML file committed we want to
  // change.
  $current_xml = @file_get_contents(__DIR__ . "/$project_name/7.x");
  $xml = new DOMDocument();
  if ($current_xml) {
    $xml->loadXML($current_xml);
  }
  else {
    // Check if there is a legacy update XML on drupal.org.
    $legacy_xml = @file_get_contents("https://updates.drupal.org/release-history/$project_name/7.x");
    if ($legacy_xml) {
      $no_legacy_xml = TRUE;
      $legacy_xml_loaded = new DOMDocument();
      $legacy_xml_loaded->loadXML($legacy_xml);
      $error = $legacy_xml_loaded->getElementsByTagName('error')->item(0);
      if (!$error) {
        $xml = $legacy_xml_loaded;
        $no_legacy_xml = FALSE;
      }
    }
    if ($no_legacy_xml) {
      // Fallback to a dummy default update XML.
      $xml->loadXML(d7security_get_default_xml($project_name, $versions));
    }
  }

  // Ensure project is marked as supported.
  $xml->getElementsByTagName('project_status')->item(0)->nodeValue = 'published';
  $xml->getElementsByTagName('link')->item(0)->nodeValue = "https://gitlab.com/d7security/$project_name";

  // Check if the release is already included and remove it, to generate it from
  // scratch.
  $releases_node = $xml->getElementsByTagName('releases')->item(0);
  $releases = $xml->getElementsByTagName('release');
  foreach ($releases as $release) {
    if ($release->getElementsByTagName('version')->item(0)->nodeValue == $tag) {
      $releases_node->removeChild($release);
      break;
    }
  }
  $target_release = $xml->createElement('release');
  d7security_insert_release($releases_node, $target_release, $versions);

  $target_release->appendChild($xml->createElement('name', "$project_name $tag"));
  $target_release->appendChild($xml->createElement('version', $tag));
  $target_release->appendChild($xml->createElement('tag', $tag));

  $target_release->appendChild($xml->createElement('version_major', $versions['major']));
  $target_release->appendChild($xml->createElement('version_patch', $versions['patch']));
  $target_release->appendChild($xml->createElement('status', 'published'));
  $target_release->appendChild($xml->createElement('release_link', "https://gitlab.com/d7security/$project_name/-/releases/$tag"));

  $project_json = json_decode(file_get_contents("https://gitlab.com/api/v4/projects/d7security%2F$project_name"), TRUE);
  if (empty($project_json)) {
    echo "Project $project_name not found on GitLab.\n";
    exit(1);
  }
  $project_id = $project_json['id'];
  $release_json = json_decode(file_get_contents("https://gitlab.com/api/v4/projects/$project_id/releases/$tag"), TRUE);
  if (empty($release_json)) {
    echo "Release $tag not found for $project_name.\n";
    exit(1);
  }
  $links = d7secrutiy_get_download_links($release_json, $project_name, $tag);
  $target_release->appendChild($xml->createElement('download_link', $links['tar_gz']));

  $target_release->appendChild($xml->createElement('date', strtotime($release_json['created_at'])));
  $tar_gz_content = file_get_contents($links['tar_gz']);
  $target_release->appendChild($xml->createElement('mdhash', md5($tar_gz_content)));
  $target_release->appendChild($xml->createElement('filesize', strlen($tar_gz_content)));

  $files = $target_release->appendChild($xml->createElement('files'));
  $tar_gz_file = $files->appendChild($xml->createElement('file'));
  $tar_gz_file->appendChild($xml->createElement('url', $links['tar_gz']));
  $tar_gz_file->appendChild($xml->createElement('archive_type', 'tar.gz'));
  $tar_gz_file->appendChild($xml->createElement('md5', md5($tar_gz_content)));
  $tar_gz_file->appendChild($xml->createElement('size', strlen($tar_gz_content)));
  $tar_gz_file->appendChild($xml->createElement('filedate', strtotime($release_json['created_at'])));

  $zip_file = $files->appendChild($xml->createElement('file'));
  $zip_content = file_get_contents($links['zip']);
  $zip_file->appendChild($xml->createElement('url', $links['tar_gz']));
  $zip_file->appendChild($xml->createElement('archive_type', 'zip'));
  $zip_file->appendChild($xml->createElement('md5', md5($zip_content)));
  $zip_file->appendChild($xml->createElement('size', strlen($zip_content)));
  $zip_file->appendChild($xml->createElement('filedate', strtotime($release_json['created_at'])));

  $terms = $target_release->appendChild($xml->createElement('terms'));
  // Always add the bugfix term, as D7Security is mainly about bugfixes.
  $bugfix_term = $terms->appendChild($xml->createElement('term'));
  $bugfix_term->appendChild($xml->createElement('name', 'Release type'));
  $bugfix_term->appendChild($xml->createElement('value', 'Bug fixes'));
  // Mark the release as security release if it contains the words "security
  // update".
  if (stripos((string) $release_json['description'], 'security update') !== FALSE) {
    $security_term = $terms->appendChild($xml->createElement('term'));
    $security_term->appendChild($xml->createElement('name', 'Release type'));
    $security_term->appendChild($xml->createElement('value', 'Security update'));
  }
  $security_coverage = $target_release->appendChild($xml->createElement('security', "Covered by D7Security's security policy"));
  $security_coverage->setAttribute('covered', '1');

  // Reload for pretty formatting.
  $xml_out = new DOMDocument();
  $xml_out->preserveWhiteSpace = FALSE;
  $xml_out->formatOutput = TRUE;
  $xml_out->loadXML($xml->saveXML());

  if (!file_exists(__DIR__ . "/$project_name")) {
    mkdir(__DIR__ . "/$project_name");
  }
  file_put_contents(__DIR__ . "/$project_name/7.x", $xml_out->saveXML());
}

/**
 * Returns the major and minor version from a tag.
 */
function d7security_get_major_minor_version(string $tag): array {
  // We don't support alpha|beta|rc versions, all releases in D7Security must be
  // stable.
  $result = preg_match('/^7\.x\-(\d+)\.(\d+)$/', $tag, $matches);
  if (empty($result)) {
    echo "Invalid tag format for $tag.\n";
    exit(1);
  }
  return [
    'major' => $matches[1],
    'patch' => $matches[2],
  ];
}

/**
 * Returns the download links for a release.
 */
function d7secrutiy_get_download_links(array $release_json, string $project_name, string $tag): array {
  $links = [];
  foreach ($release_json['assets']['links'] as $link) {
    if ($link['name'] == "$project_name-$tag.tar.gz") {
      $links['tar_gz'] = $link['url'];
    }
    if ($link['name'] == "$project_name-$tag.zip") {
      $links['zip'] = $link['url'];
    }
  }
  if (empty($links['tar_gz']) || empty($links['zip'])) {
    echo "Download links missing for $project_name $tag.\n";
    exit(1);
  }
  return $links;
}

/**
 * Returns a dummy default XML.
 */
function d7security_get_default_xml(string $project_name, array $versions): string {
  return <<<XML
<?xml version="1.0" encoding="utf-8"?>
<project xmlns:dc="http://purl.org/dc/elements/1.1/">
  <title>$project_name</title>
  <short_name>$project_name</short_name>
  <type>project_module</type>
  <api_version>7.x</api_version>
  <recommended_major>{$versions['major']}</recommended_major>
  <supported_majors>{$versions['major']}</supported_majors>
  <default_major>{$versions['major']}</default_major>
  <project_status>published</project_status>
  <link>https://gitlab.com/d7security/$project_name</link>
  <terms>
    <term>
      <name>Projects</name>
      <value>Modules</value>
    </term>
    <term>
      <name>Maintenance status</name>
      <value>Minimally maintained</value>
    </term>
    <term>
      <name>Development status</name>
      <value>Under active development</value>
    </term>
  </terms>
  <releases>
  </releases>
</project>
XML;
}

/**
 * Inserts the new target release node at the correct position.
 */
function d7security_insert_release(\DOMNode $releases_node, \DOMElement $target_release, array $target_versions): void {
  $releases = $releases_node->getElementsByTagName('release');
  foreach ($releases as $release) {
    $current_versions = d7security_get_major_minor_version($release->getElementsByTagName('version')->item(0)->nodeValue);
    if ($current_versions['major'] < $target_versions['major']) {
      $releases_node->insertBefore($target_release, $release);
      return;
    }
    if ($current_versions['patch'] < $target_versions['patch']) {
      $releases_node->insertBefore($target_release, $release);
      return;
    }
  }
  $releases_node->insertBefore($target_release, $releases_node->lastChild);
}
